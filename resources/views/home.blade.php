@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <a href="{{route('create')}}" class="btn btn-primary">Add New</a>
            <div class="card mt-4">
                <div class="card-header">{{ __('Reminder App') }}</div>

                <div class="card-body">
                    <table class="table">
                      <thead>
                        <tr>
                          <th scope="col">Title</th>
                          <th scope="col">Description</th>
                          <th scope="col">Time</th>
                          <th scope="col">Action</th>
                        </tr>
                      </thead>
                      <tbody class="reminder-list">
                        @foreach($reminders as $reminder)
                          <tr>
                              <td>{{$reminder->title}}</td>
                              <td>{{$reminder->description}}</td>
                              <td>{{$reminder->date_time}}</td>
                              <td>
                                <a href="{{route('edit',$reminder->id)}}" class="btn btn-primary">Edit</a>
                                <a href="javascript:void(0);"  data-reminder-id="{{ $reminder->id }}" class="btn btn-primary delete-reminder">Delete</a>
                              </td>

                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        // CSRF Token
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // Delete post
        $('.delete-reminder').click(function() {
            var reminderId = $(this).data('reminder-id');

            $.ajax({
                type: 'POST',
                url: '/delete/' + reminderId,
                success: function(data) {
                    if (data.success) {
                        // Remove the deleted reminder from the list
                        $('[data-reminder-id="' + reminderId + '"]').closest('tr').remove();
                    }
                }
            });
        });
    });
</script>
@endsection

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Reminder;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $userId = \Auth::user()->id;
        $reminders = Reminder::where('user_id',$userId)->get();
        return view('home')->with('reminders',$reminders);
    }

    public function create()
    {
        return view('create');
    }

    public function store(Request $request)
    {
        // echo '<pre>';print_r($request->all());exit();
        $request->validate([
            'title' => 'required|string|max:50',
            'description' => 'required',
            'datetime' => 'required',
        ]);

        $userId = \Auth::user()->id;
        $reminder = new Reminder();
        $reminder->user_id = $userId;
        $reminder->title = $request->title;
        $reminder->description = $request->description;
        $reminder->date_time = $request->datetime;
        $reminder->save();

        return  \Redirect::route('home');

    }

    public function edit($id)
    {
        $reminder = Reminder::find($id);
        return view('edit')->with('reminder',$reminder);
    }

    public function update(Request $request,$id)
    {
        $request->validate([
            'title' => 'required|string|max:50',
            'description' => 'required',
            'datetime' => 'required',
        ]);

        $userId = \Auth::user()->id;
        $reminder = Reminder::find($id);
        $reminder->user_id = $userId;
        $reminder->title = $request->title;
        $reminder->description = $request->description;
        $reminder->date_time = $request->datetime;
        $reminder->save();

        return  \Redirect::route('home');
    }

    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $reminder = Reminder::findOrFail($id);
            $reminder->delete();

            return response()->json(['success' => true]);
        }
    }

}

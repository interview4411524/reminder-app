<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Http\Controllers\DateTime;
use App\Models\Reminder;
use App\Models\User;
use Mail;

class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:mail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $date = date('Y-m-d H:i');
        $dt = new \DateTime($date, new \DateTimeZone('UTC'));
        $dt->setTimezone(new \DateTimeZone('Asia/Kolkata'));
        $new_date = $dt->format('Y-m-d H:i');
        
        $reminders = Reminder::where('date_time','=',$new_date)->get();
        // echo '<pre>';print_r($reminders);exit();
        foreach ($reminders as $reminder) {
            $user = User::find($reminder->user_id);
            Mail::send('email_template', array(
                'reminder'        => $reminder,
                'user'           => $user
            ), function($message) use ($user,$reminder){
                $message->to($user->email)->subject('Your reminder for '.$reminder->title);
            });

        }

    }
}
